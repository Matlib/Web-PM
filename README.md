<big><b>Web::PM – lightweight Perl appserver</b></big>

The goal of this project is to provide a very lightweight appserver for Perl applications that does not drag 1990s legacy like templates, dubious features like routing, tons of heavy dependencies, “syntactic sugar” wrappers and so on. 

Current version compiles with bare perl 5.16+.

<h1>COMPILING</h1>

Automatic compilation with <a href="https://metacpan.org/pod/ExtUtils::MakeMaker">MakeMaker</a> and <a href="https://metacpan.org/pod/ExtUtils::PkgConfig">PkgConfig</a> is recommended. If you clone or download directly from git, you need to build the package first:

```
perl Makefile.PL
make dist
```

Having got the package you may install either with `cpanm Web-PM-*.tar.gz` (if available) or with the following commands:

```
tar -vxpf Web-PM-*.tar.gz
cd Web-PM-*[0-9]
perl Makefile.PL
make test
make install
```

<i><b>Note:</b></i> Common recipes for customizing the build can be found in <a href="https://perldoc.pl/ExtUtils::MakeMaker::FAQ">MakeMaker's FAQ</a>.

<i><b>Note for Solaris:</b></i> MakeMaker will try to mimic the way Perl was built, that is with the Sun Studio compiler. If you do not have it installed, or just want to use something else, then you need to provide your own commands to build the module. For GCC that's ```perl Makefile.PL CC=gcc CCCDLFLAGS=-fPIC OPTIMIZE=-O2 LD=gcc```.

<h1>AUTHORS</h1>

Matt Latusek, matlib@matlibhax.com 
