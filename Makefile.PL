#!/usr/bin/env perl
use strict;
use warnings;

use ExtUtils::MakeMaker;
use ExtUtils::PkgConfig;

WriteMakefile(
    NAME               => 'Web::PM',
    VERSION            => '0.00',
    MIN_PERL_VERSION   => v5.10,
    ABSTRACT           => 'Perl AppServer Process Manager',
    LICENSE            => 'unrestricted',
    AUTHOR             => ['Matt Latusek <matlib@matlibhax.com>'],
    CONFIGURE_REQUIRES => { 'ExtUtils::PkgConfig' => '1.12' },
    PREREQ_PM          => { 'DynaLoader' => 0, },
    META_MERGE         => {
        'meta-spec' => { version => 2 },
        resources   => {
            repository => {
                type => 'git',
                url  => 'https://gitlab.com/Matlib/Web-PM.git',
                web  => 'https://gitlab.com/Matlib/Web-PM',
            },
            bugtracker => {
                mailto => 'matlib@cpan.org',
                web    => 'https://gitlab.com/Matlib/Web-PM/-/issues',
            },
        },
    },

    dist => {
        DIST_CP => 'cp',
        PREOP   => 'perl insert-version.PL',
    },

    H      => ['PM.h'],
    OBJECT => [ 'PM.o', 'string.o', 'io.o', 'fcgi.o' ],
);
